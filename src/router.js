import Vue from 'vue'
import Router from 'vue-router'

import MainPage from './components/MainPage'

Vue.use(Router)

const router = new Router({
  routes: [
    { path: '/mainPage', component: MainPage }
  ]
})

export default router
